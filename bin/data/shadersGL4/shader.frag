#version 150

// this is how we receive the texture
uniform sampler2DRect tex1;
uniform sampler2DRect tex2;
uniform sampler2DRect tex3;

//uniform sampler2DRect noise_texture;

uniform float main_opacity;

uniform float opacity_1;
uniform float contrast_1;
uniform float saturation_1;
uniform float scale_1;
uniform int x_pos_1;
uniform int y_pos_1;

uniform float opacity_2;
uniform float contrast_2;
uniform float saturation_2;
uniform float scale_2;
uniform int x_pos_2;
uniform int y_pos_2;

uniform float key_value_threshold;
uniform int key_hue_threshold;

uniform int blendmode;
uniform int old_blendmode;
uniform float blendmode_transition;

//in vec2 brightContrast;
in vec2 texCoordVarying;

out vec4 outputColor;

// this is coming from our C++ code
//uniform float mouseX;
//uniform float mouseY;

/*
** Hue, saturation, luminance
*/

vec3 RGBToHSL(vec3 color)
{
	vec3 hsl; // init to 0 to avoid warnings ? (and reverse if + remove first part)

	float fmin = min(min(color.r, color.g), color.b);    //Min. value of RGB
	float fmax = max(max(color.r, color.g), color.b);    //Max. value of RGB
	float delta = fmax - fmin;             //Delta RGB value

	hsl.z = (fmax + fmin) / 2.0; // Luminance

	if (delta == 0.0)		//This is a gray, no chroma...
	{
		hsl.x = 0.0;	// Hue
		hsl.y = 0.0;	// Saturation
	}
	else                                    //Chromatic data...
	{
		if (hsl.z < 0.5)
			hsl.y = delta / (fmax + fmin); // Saturation
		else
			hsl.y = delta / (2.0 - fmax - fmin); // Saturation

		float deltaR = (((fmax - color.r) / 6.0) + (delta / 2.0)) / delta;
		float deltaG = (((fmax - color.g) / 6.0) + (delta / 2.0)) / delta;
		float deltaB = (((fmax - color.b) / 6.0) + (delta / 2.0)) / delta;

		if (color.r == fmax )
			hsl.x = deltaB - deltaG; // Hue
		else if (color.g == fmax)
			hsl.x = (1.0 / 3.0) + deltaR - deltaB; // Hue
		else if (color.b == fmax)
			hsl.x = (2.0 / 3.0) + deltaG - deltaR; // Hue

		if (hsl.x < 0.0)
			hsl.x += 1.0; // Hue
		else if (hsl.x > 1.0)
			hsl.x -= 1.0; // Hue
	}

	return hsl;
}

vec3 RGBToHSV(vec3 color)
{
	vec3 hsv;

	float fmin = min(min(color.r, color.g), color.b);	 // Min. value of RGB
	float fmax = max(max(color.r, color.g), color.b);    //Max. value of RGB

	if (fmin == fmax) {
		hsv.x = 0;
	} else  if (fmax == color.r) {
		hsv.x = 60 * (0 + (color.g - color.b)/(fmax-fmin));
	} else  if (fmax == color.g) {
		hsv.x = 60 * (2 + (color.b - color.r)/(fmax-fmin));
	} else {
		hsv.x = 60 * (4 + (color.r - color.g)/(fmax-fmin));
	}

	while (hsv.x < 0) {
		hsv.x += 360;
	}

	if (fmax == 0) {
		hsv.y = 0;
	} else {
		hsv.y = (fmax - fmin)/fmax;
	}

	hsv.z = fmax;

	return hsv;
}


vec3 HSLtoHSV(vec3 hsl)
{
	// rules from https://en.wikipedia.org/wiki/HSL_and_HSV#Conversion_HSL_to_HSV
	vec3 hsv;
	hsv.x = hsl.x;
	hsv.z = hsl.z + hsl.y * min(hsl.z, 1-hsl.z);
	if (hsv.z == 0) {
		hsv.y = 0;
	} else {
		hsv.y = 2 - 2 * hsl.z / hsv.z;
	}
	return hsv;
}

float HueToRGB(float f1, float f2, float hue)
{
	if (hue < 0.0)
		hue += 1.0;
	else if (hue > 1.0)
		hue -= 1.0;
	float res;
	if ((6.0 * hue) < 1.0)
		res = f1 + (f2 - f1) * 6.0 * hue;
	else if ((2.0 * hue) < 1.0)
		res = f2;
	else if ((3.0 * hue) < 2.0)
		res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
	else
		res = f1;
	return res;
}

vec3 HSLToRGB(vec3 hsl)
{
	vec3 rgb;

	if (hsl.y == 0.0)
		rgb = vec3(hsl.z); // Luminance
	else
	{
		float f2;

		if (hsl.z < 0.5)
			f2 = hsl.z * (1.0 + hsl.y);
		else
			f2 = (hsl.z + hsl.y) - (hsl.y * hsl.z);

		float f1 = 2.0 * hsl.z - f2;

		rgb.r = HueToRGB(f1, f2, hsl.x + (1.0/3.0));
		rgb.g = HueToRGB(f1, f2, hsl.x);
		rgb.b= HueToRGB(f1, f2, hsl.x - (1.0/3.0));
	}

	return rgb;
}

/*
** Contrast, saturation, brightness
** Code of this function is from TGM's shader pack
** http://irrlicht.sourceforge.net/phpBB2/viewtopic.php?t=21057
*/

// For all settings: 1.0 = 100% 0.5=50% 1.5 = 150%
vec3 ContrastSaturationBrightness(vec3 color, float con, float sat, float brt)
{
	// Increase or decrease theese values to adjust r, g and b color channels seperately
	const float AvgLumR = 0.5;
	const float AvgLumG = 0.5;
	const float AvgLumB = 0.5;

	const vec3 LumCoeff = vec3(0.2125, 0.7154, 0.0721);

	vec3 AvgLumin = vec3(AvgLumR, AvgLumG, AvgLumB);
	vec3 brtColor = color * brt;
	vec3 intensity = vec3(dot(brtColor, LumCoeff));
	vec3 satColor = mix(intensity, brtColor, sat);
	vec3 conColor = mix(AvgLumin, satColor, con);
	return conColor;
}

vec3 BlendColor(vec3 layer_1, vec3 layer_2, int blendmode) {
	vec3 result;

	switch (blendmode) {
		case 0:
			// 	0 - basic superimposition
			for (int i = 0; i < 3; ++i) {
				result[i] = (layer_1[i] + layer_2[i]) / 2;
			}
		break;

		case 1:
			// 	1 - multiply a * b
			for (int i = 0; i < 3; ++i) {
				result[i] = layer_1[i] * layer_2[i];
			}
		break;

		case 2:
			// 2 - screen 1 - (1 - a) * (1 - b)
			for (int i = 0; i < 3; ++i) {
				result[i] = 1 - (1 - layer_1[i]) * (1 - layer_2[i]);
			}
		break;

		case 3:
			// 3 - darken min(a, b)
			for (int i = 0; i < 3; ++i) {
				result[i] = min(layer_1[i], layer_2[i]);
			}
		break;

		case 4:
			// 4 - lighten max(a, b)
			for (int i = 0; i < 3; ++i) {
				result[i] = max(layer_1[i], layer_2[i]);
			}
		break;

		case 5:
			// 5 - difference abs(a - b)
			for (int i = 0; i < 3; ++i) {
				result[i] = abs(layer_1[i] - layer_2[i]);
			}
		break;

		case 6:
			// 6 - negation 1 - abs(1 - a - b)
			for (int i = 0; i < 3; ++i) {
				result[i] = 1 - abs(1 - layer_1[i] - layer_2[i]);
			}
		break;

		case 7:
			// 	7 - exclusion a + b - 2 * a * b
			for (int i = 0; i < 3; ++i) {
				result[i] = layer_1[i] + layer_2[i] - 2 * layer_1[i] * layer_2[i];
			}
		break;

		case 8:
			// 8 - overlay a < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b))
			for (int i = 0; i < 3; ++i) {
				float a = layer_1[i];
				float b = layer_2[i];
				result[i] = a < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b));
			}
		break;

		case 9:
			// 9 - hard light b < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b))
			for (int i = 0; i < 3; ++i) {
				float a = layer_1[i];
				float b = layer_2[i];
				result[i] = b < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b));
			}
		break;

		case 10:
			// 10 - soft light b < .5 ? (2 * a * b + a * a * (1 - 2 * b)) : (sqrt(a) * (2 * b - 1) + (2 * a) * (1 - b))
			for (int i = 0; i < 3; ++i) {
				float a = layer_1[i];
				float b = layer_2[i];
				result[i] = b < .5 ? (2 * a * b + a * a * (1 - 2 * b)) : (sqrt(a) * (2 * b - 1) + (2 * a) * (1 - b));
			}
		break;

		case 11:
			// 11 - dodge a / (1 - b)
			for (int i = 0; i < 3; ++i) {
				float a = layer_1[i];
				float b = layer_2[i];
				result[i] = a / (1 - b);
			}
		break;

		case 12:
			// 12 - burn 1 - (1 - a) / b
			for (int i = 0; i < 3; ++i) {
				float a = layer_1[i];
				float b = layer_2[i];
				result[i] = 1 - (1 - a) / b;
			}
		break;
	}

	return result;
}

void main()
{

    vec4 channel_1 = texture2DRect(tex1, vec2((texCoordVarying.x - x_pos_1) * scale_1, (texCoordVarying.y - y_pos_1) * scale_1));
    vec4 channel_2 = texture2DRect(tex2, vec2((texCoordVarying.x - x_pos_2) * scale_2, (texCoordVarying.y - y_pos_2) * scale_2));

	vec4 channel_3 = texture2DRect(tex3, texCoordVarying);

//vec4 channel_4 = texture2DRect(tex4, noiseTexCoord);

	float sin_val = opacity_1;
	
	vec3 color;
	vec3 color_layer_1, color_layer_2;
	vec3 background_color = vec3(0,0,0);

	color_layer_1 = mix(background_color, channel_1.rgb, opacity_1);
	color_layer_1 = ContrastSaturationBrightness(color_layer_1,contrast_1,saturation_1,1);

	color_layer_2 = mix(background_color, channel_2.rgb, opacity_2);
	color_layer_2 = ContrastSaturationBrightness(color_layer_2,contrast_2,saturation_2,1);

	
//	// UNCOMMENT TO GET THE THREE PANELS + HAND
//	if (texCoordVarying.y/panel_height < 0.3) {
//
//		color_layer_1 = ContrastSaturationBrightness(channel_1.rgb,1,1,1);
//		color_layer_2 = ContrastSaturationBrightness(channel_4.rgb,(opacity_1/2)+1,1,opacity_1/2);
//		color_layer_2 = mix(vec3(0,0,0),color_layer_2,opacity_1);
//
//	}
//	else if (texCoordVarying.y/panel_height < 0.6) {
//
//		color_layer_1 = ContrastSaturationBrightness(channel_2.rgb,1,1,1);
//
//		color_layer_2 = ContrastSaturationBrightness(channel_4.rgb,(opacity_2/2)+1,1,opacity_2/2);
//		color_layer_2 = mix(vec3(0,0,0),color_layer_2,opacity_2);
//
//	}
//	else {
//
//		color_layer_1 = ContrastSaturationBrightness(channel_3.rgb,1,1,1);
//
//		color_layer_2 = ContrastSaturationBrightness(channel_4.rgb,(opacity_3/2)+1,1,opacity_3/2);
//		color_layer_2 = mix(vec3(0,0,0),color_layer_2,opacity_3);
//	}

	
	/*
	1 - multiply a * b
	2 - screen 1 - (1 - a) * (1 - b)
	3 - darken min(a, b)
	4 - lighten max(a, b)
	5 - difference abs(a - b)
	6 - negation 1 - abs(1 - a - b)
	7 - exclusion a + b - 2 * a * b
	8 - overlay a < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b))
	9 - hard light b < .5 ? (2 * a * b) : (1 - 2 * (1 - a) * (1 - b))
	10 - soft light b < .5 ? (2 * a * b + a * a * (1 - 2 * b)) : (sqrt(a) * (2 * b - 1) + (2 * a) * (1 - b))
	11 - dodge a / (1 - b)
	12 - burn 1 - (1 - a) / b
	*/
	
	// blendmode_transition to smooth the changes of blending mode
	// value is send from the panel
 	
	if (blendmode_transition == 1) {
	    if(blendmode == 0) {
		color = mix(color_layer_1,color_layer_2,main_opacity);
	    } else {
		color = BlendColor(color_layer_1,color_layer_2,blendmode);
    	}
	} else {
	   vec3 new_color = BlendColor(color_layer_1,color_layer_2,blendmode);
	   vec3 old_color = BlendColor(color_layer_1,color_layer_2,old_blendmode);
	   color = mix(color_layer_1,color_layer_2,blendmode_transition);
	}

    // note:: CSB goes from effect to negative (if mouseX from 1 to -1).
	
//	// HUE-VALUE matte
//	vec3 hsv_color = RGBToHSV(color);
//	vec3 tmp_color = color;
//	if (hsv_color.x > key_hue_threshold) {
//		if (hsv_color.z > key_value_threshold) {
//				tmp_color = color;
//		} else {
//			tmp_color = vec3(0,0,0);
//		}
//	} else {
//		tmp_color = vec3(0,0,0);
//	}
//	color = tmp_color;

	// key_value_threshold
    // key_hue_threshold;

    outputColor = vec4(color,1); //channel_2.r);

}