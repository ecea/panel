#include "ofApp.h"

#define INTERFACE
#define ARDUINO_
#define WRITE_INFO_

#define REBOUNCE_DELAY_MS 10
#define Math_PI 3.14159265358979323846  /* pi */

static int prev_frame = 0;
static bool db = false;

//--------------------------------------------------------------
void ofApp::setup()
{
	animationTime = 100;
	number_of_channels = 2;

	ofSetVerticalSync(true); // to avoid tearing. Try to disable to build glitches?
	shader.load("shadersGL4/shader");

	// fbo_frame is used for glitch lines
	// :: has to be bigger than the kanava fbo.
	fbo_frame.allocate(1920, 1080, GL_RGB);
	fbo_frame.begin();
	ofClear(0, 0, 0);
	fbo_frame.end();

	main_fbo.allocate(1920, 1080, GL_RGB);

	string src = "rushes/fouquets.mov"; // main source file
	list_of_kanava = new kanava[number_of_channels];

	for (int i = 0; i < number_of_channels; ++i) {
		list_of_kanava[i].build(1280, 1080, src);
	}

	// BASIC PARAMETERS
	blendmode = 0;
	old_blendmode = 0;
	blendmode_transition_steps = 20;
	blendmode_transition_actualStep = 20;

	main_opacity = 50;
	key_hue = 120; key_value = 0.5;

#ifdef INTERFACE

	kanava_gui = new KanavaFolder[number_of_channels];
	kanava_variables = new channelVariable[number_of_channels];
	int x = 280, y = 0;
	for (int i = 0; i < number_of_channels; ++i) {

		kanava_variables[i].init();

		kanava_gui[i].geometry = new ofxDatGuiFolder("Color " + ofToString(i + 1));
		kanava_gui[i].geometry->addSlider("Opacity " + ofToString(i + 1), 0, 100);
		kanava_gui[i].geometry->addSlider("Contrast " + ofToString(i + 1), 0, 200);
		kanava_gui[i].geometry->addSlider("Saturation " + ofToString(i + 1), 0, 100);

		kanava_gui[i].geometry->addSlider("X pos " + ofToString(i + 1), -1920, 1920, 0);
		kanava_gui[i].geometry->addSlider("Y pos " + ofToString(i + 1), -1080, 1080, 0);
		kanava_gui[i].geometry->addSlider("Scale " + ofToString(i + 1), 0.1, 200, 100);

		kanava_gui[i].geometry->addSlider("Fbo width" + ofToString(i + 1), 0, 1280, 1280);
		kanava_gui[i].geometry->addSlider("Inside X pos" + ofToString(i + 1), -1280, 1280, 0);
		kanava_gui[i].geometry->addSlider("Crop X" + ofToString(i + 1), -1280, 1280, 0);
		kanava_gui[i].geometry->onSliderEvent(this, &ofApp::onSliderEvent);

		kanava_gui[i].geometry->setPosition(x * i, y);
		kanava_gui[i].geometry->expand();

		parametersAutomaton = new parameterGUIAutomaton[1];

		kanava_gui[i].pos_x_automaton.setupAutomaton("pos_x " + ofToString(i), x * i, y + 9 * 30, 0, 100, 10);
		kanava_gui[i].pos_x_automaton.folder->onToggleEvent(this, &ofApp::onToggleEvent);
		kanava_gui[i].pos_x_automaton.folder->onSliderEvent(this, &ofApp::onSliderEvent);
	}
#endif

#ifdef ARDUINO
  // INIT ARDUINO
  ard.connect("COM5", 57600);
  ard.sendFirmwareVersionRequest();
  ofAddListener(ard.EInitialized, this, &ofApp::setupArduino);
#endif

  bSetupArduino = false; // flag so we setup arduino when its ready, you don't need to touch this :)
  
  font.load("fonts/Datalegreya-Thin.otf", 20);

}

//--------------------------------------------------------------
void ofApp::update()
{
	ofSetColor(255, 255, 255, 255);
	float time = ofGetElapsedTimeMillis();

	for (int i = 0; i < number_of_channels; ++i) {
		list_of_kanava[i].update();

#ifdef INTERFACE
		kanava_gui[i].geometry->update();
		kanava_gui[i].pos_x_automaton.folder->update();

		if ((kanava_gui[i].pos_x_automaton.launch == true) && (time > animationTime)) {
			kanava_variables[i].x_pos += kanava_gui[i].pos_x_automaton.step_value * (kanava_gui[i].pos_x_automaton.reverse ? -1 : 1);

			if ((kanava_variables[i].x_pos < 0) || (kanava_variables[i].x_pos >= 1920)) {
				if (kanava_gui[i].pos_x_automaton.loop) {
					kanava_gui[i].pos_x_automaton.reverse = !kanava_gui[i].pos_x_automaton.reverse;
				}
			}

			animationTime += kanava_gui[i].pos_x_automaton.speed_value + ofGetFrameRate();
		}
#endif

    if (blendmode_transition_actualStep < blendmode_transition_steps) {
        blendmode_transition_actualStep++;
    }


};

#ifdef ARDUINO
  updateArduino();
#endif


}

//--------------------------------------------------------------
void ofApp::draw()
{
	ofBackground(120,120,0);
    ofSetColor(255,255,255);
	// Basic info, written in the headbar
	string windowName = ofToString(ofGetFrameRate());
	ofSetWindowTitle(windowName);

	// Setting up the main_fbo (shader)
	main_fbo.begin();
	ofClear(0, 0, 0);

	shader.begin();
	ofPushMatrix();

	shader.setUniform1i("blendmode", blendmode);
	shader.setUniform1i("old_blendmode", old_blendmode);
	shader.setUniform1f("blendmode_transition", (float)blendmode_transition_actualStep/blendmode_transition_steps);

	shader.setUniform1f("main_opacity", (float)main_opacity / 100);

	shader.setUniform1i("key_hue_threshold", key_hue);
	shader.setUniform1f("key_value_threshold", key_value);

	// Passing texture, opacity and contrast for each channel
	for (int i = 0; i < number_of_channels; ++i) {
		shader.setUniformTexture("tex" + ofToString(i + 1), list_of_kanava[i].getTexture(), i + 1);
		shader.setUniform1f("opacity_" + ofToString(i + 1), (float)kanava_variables[i].opacity / 100);
		shader.setUniform1f("contrast_" + ofToString(i + 1), (float)kanava_variables[i].contrast / 100);
		shader.setUniform1f("saturation_" + ofToString(i + 1), (float)kanava_variables[i].saturation / 100);

		shader.setUniform1i("x_pos_" + ofToString(i + 1), (int)kanava_variables[i].x_pos);
		shader.setUniform1i("y_pos_" + ofToString(i + 1), (int)kanava_variables[i].y_pos);
		shader.setUniform1f("scale_" + ofToString(i + 1), (float)kanava_variables[i].scale / 100);

	}

	// used for texcoord - independant from list of kanava
	fbo_frame.draw(0, 0);

	ofPopMatrix();

	shader.end();
	main_fbo.end();

	ofPushMatrix();
	ofScale(2);

	main_fbo.draw(300, 0);

	ofPopMatrix();


#ifdef INTERFACE
	for (int i = 0; i < number_of_channels; ++i) {
		kanava_gui[i].geometry->draw();
		kanava_gui[i].pos_x_automaton.folder->draw();
	}
#endif
#ifdef WRITE_INFO
	font.drawString(ofToString(ofGetFrameRate()) + "\nspeeds: {" + ofToString(speed_ch1) + "," + ofToString(speed_ch2) + "," + ofToString(speed_ch3) + "}\nopBC: {" + ofToString(opBC_ch1) + "," + ofToString(opBC_ch2) + "," + ofToString(opBC_ch3) + "} \nblendmode::" + blendName + " \nmain_opacity::" + ofToString(main_opacity), 100, 100);
	if (bSetupArduino) {
		string to_write = "";
		for (int i = 0; i < buttonNumber; ++i) {
			to_write += ofToString(i) + ":"+ofToString(list_of_buttons[i].clickCount) + ",  ";
			if (i == 4) {
				to_write += "\n test ";
			}
		}
		font.drawString(to_write, 300, 100);
	}
#endif
    
}

//--------------------------------------------------------------
void ofApp::exit()
{
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch (key)
	{
	case 'o':
		cout << "whatever is needed" << endl;
		break;
	default:
		break;
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
    ofVec2f mouse(x,y);
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    ofVec2f mouse(x,y);
	ofShowCursor();
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
   
}

#ifdef ARDUINO
/* ************ */
/* ARDUINO PART */
/* ************ */

//--------------------------------------------------------------
void ofApp::setupArduino(const int & version) {
	
  // remove listener because we don't need it anymore
  ofRemoveListener(ard.EInitialized, this, &ofApp::setupArduino);

  // it is now safe to send commands to the Arduino
  bSetupArduino = true;
  
  // print firmware name and version to the console
  ofLogNotice() << ard.getFirmwareName(); 
  ofLogNotice() << "firmata v" << ard.getMajorFirmwareVersion() << "." << ard.getMinorFirmwareVersion();
  
  // Note: pins A0 - A5 can be used as digital input and output.
  // Refer to them as pins 14 - 19 if using StandardFirmata from Arduino 1.0.
  // If using Arduino 0022 or older, then use 16 - 21.
  // Firmata pin numbering changed in version 2.3 (which is included in Arduino 1.0)
  
  buttonNumber = 10;
  list_of_buttons = new buttonStruct[buttonNumber];
  for (int i = 0; i < buttonNumber; i++)
  {
	  list_of_buttons[i].isPressed = false;
	  list_of_buttons[i].rebounceTime = 0;
	  list_of_buttons[i].clickCount = 0;
  }

  //// Initialiser les numros spcifiques des entres utilises
  list_of_buttons[0].pinNumber = 2;
  list_of_buttons[1].pinNumber = 3;
  list_of_buttons[2].pinNumber = 4;
  list_of_buttons[3].pinNumber = 5;
  list_of_buttons[4].pinNumber = 6;
  list_of_buttons[5].pinNumber = 7;
  list_of_buttons[6].pinNumber = 10;
  list_of_buttons[7].pinNumber = 11;
  list_of_buttons[8].pinNumber = 12;
  list_of_buttons[9].pinNumber = 13;

  //// Initialiser les PIN de tous les boutons  grer
  for (int i = 0; i < buttonNumber; i++)
	{
	  ard.sendDigitalPinMode(list_of_buttons[i].pinNumber, ARD_INPUT);
	 // pinMode(list_of_buttons[i].entreeDigitale, INPUT);
  }

  // set pins D2 and A5 to digital input
  //ard.sendDigitalPinMode(2, ARD_INPUT);
  //ard.sendDigitalPinMode(3, ARD_INPUT);
  //ard.sendDigitalPinMode(4, ARD_INPUT);
  //ard.sendDigitalPinMode(5, ARD_INPUT);
  //ard.sendDigitalPinMode(6, ARD_INPUT);
  //ard.sendDigitalPinMode(19, ARD_INPUT);  // pin 21 if using StandardFirmata from Arduino 0022 or older

  // set pin A0 to analog input
  ard.sendAnalogPinReporting(0, ARD_ANALOG);
  ard.sendAnalogPinReporting(1, ARD_ANALOG);
  ard.sendAnalogPinReporting(2, ARD_ANALOG);
  ard.sendAnalogPinReporting(3, ARD_ANALOG);
  ard.sendAnalogPinReporting(4, ARD_ANALOG);
  ard.sendAnalogPinReporting(5, ARD_ANALOG);
  ard.sendAnalogPinReporting(6, ARD_ANALOG);
  ard.sendAnalogPinReporting(7, ARD_ANALOG);

    // set pin D13 as digital output
  // ard.sendDigitalPinMode(13, ARD_OUTPUT);
  // set pin A4 as digital output
  ard.sendDigitalPinMode(18, ARD_OUTPUT);  // pin 20 if using StandardFirmata from Arduino 0022 or older
  
  // set pin D11 as PWM (analog output)
  // ard.sendDigitalPinMode(11, ARD_PWM);
  
  // attach a servo to pin D9
  // servo motors can only be attached to pin D3, D5, D6, D9, D10, or D11
  // ard.sendServoAttach(9);
  
  // Listen for changes on the digital and analog pins
  //ofAddListener(ard.EDigitalPinChanged, this, &ofApp::digitalPinChanged);
  ofAddListener(ard.EAnalogPinChanged, this, &ofApp::analogPinChanged);    
}

//--------------------------------------------------------------
void ofApp::updateArduino(){
  // update the arduino, get any data or messages.
    // the call to ard.update() is required
	ard.update();

	// do not send anything until the arduino has been set up
	if (bSetupArduino) {
        // fade the led connected to pin D11
		for (int i = 0; i < buttonNumber; ++i) {
			stateOfButton(&(list_of_buttons[i]));
		}

		/*if (ard.getDigital(4)) {
			list_of_buttons[3].clickCount++;
		}*/

		// REVERSE SPEED
		if (list_of_buttons[0].isPressed) {
			factor_speed_ch1 = -factor_speed_ch1;
			speed_ch1 = factor_speed_ch1*speed_ch1;
			hap_ch1.setSpeed(speed_ch1);
		}
		if (list_of_buttons[1].isPressed) {
			speed_ch2 = factor_speed_ch2*speed_ch2;
			hap_ch2.setSpeed(speed_ch2);
		}

		// START - STOP button
		if (list_of_buttons[2].isPressed) {
			if (hap_ch1.isPlaying()) {
				hap_ch1.stop();
			}
			else {
				hap_ch1.play();
			}
		}
		if (list_of_buttons[3].isPressed) {
			if (hap_ch2.isPlaying()) {
				hap_ch2.stop();
			}
			else {
				hap_ch2.play();
			}
		}
		if (list_of_buttons[4].isPressed) {
			if (hap_ch3.isPlaying()) {
				hap_ch3.stop();
			}
			else {
				hap_ch3.play();
			}
			if (hap_ch1.isPlaying()) {
				hap_ch1.stop();
			}
			else {
				hap_ch1.play();
			}
			if (hap_ch2.isPlaying()) {
				hap_ch2.stop();
			}
			else {
				hap_ch2.play();
			}
		}

		// START AGAIN CHANNEL
		if (list_of_buttons[5].isPressed) {
			hap_ch1.setFrame(0);
		}
		if (list_of_buttons[6].isPressed) {
			hap_ch2.setFrame(0);
		}
		if (list_of_buttons[7].isPressed) {
			hap_ch1.setFrame(0);
			hap_ch2.setFrame(0);
			hap_ch3.setFrame(0);
		}

	}

}

//--------------------------------------------------------------
void ofApp::digitalPinChanged(const int & pinNum) {
    // do something with the digital input. here we're simply going to print the pin number and
    // value to the screen each time it changes
    /*buttonState = "digital pin: " + ofToString(pinNum) + " = " + ofToString(ard.getDigital(pinNum));
	if (pinNum == 2) {
		count_1++;
	}
	if ((pinNum == 3) && (!ard.getDigital(3))) {
		count_2++;
	}
	if (pinNum == 4) {
		count_3++;
	}
	if (pinNum == 5) {
		count_4++;
	}
	if (pinNum == 6) {
		count_5++;
	}
	
	// for blendmode
	
	if(clicked) {
	    old_blendmode = blendmode;
	    blendmode++;
	    blendmode_transition_actualStep = 0;
	}
	
	*/
	//cout << "digital pin: " + ofToString(pinNum) + " = " + ofToString(ard.getDigital(pinNum));
}

void ofApp::stateOfButton(buttonStruct *someButton) {
	bool signalState = !ard.getDigital(someButton->pinNumber);
	if (signalState) {
		if (someButton->rebounceTime != 0) {
			unsigned long elapsedTime = ofGetElapsedTimeMillis() - someButton->rebounceTime;

			if (elapsedTime >= REBOUNCE_DELAY_MS) {
				someButton->isPressed = true;
				someButton->rebounceTime = 0;
				someButton->clickCount++;
			}
		}
		else if (!someButton->isPressed) {
			someButton->rebounceTime = ofGetElapsedTimeMillis();
		}
	}
	else {
		if (someButton->isPressed) {
			someButton->isPressed = false;
		}
		else {
			someButton->rebounceTime = 0;
		}
	}
}


void smoothing_speed(float x, int max) {
    return max * sin(x*x) * sin (x*x);
}

//--------------------------------------------------------------
void ofApp::analogPinChanged(const int & pinNum) {
    
    potValue = "analog pin: " + ofToString(pinNum) + " = " + ofToString(ard.getAnalog(pinNum));
    
    
    /* to check */
    if (pinNum == 44) {
		potar_0 = ofMap(ard.getAnalog(pinNum), 0,1024,-Math_PI/2,Math_PI/2);
		//if ((int)potar_0*10 != (int)speed_ch1 * 10) {
		potar_0 = roundf(potar_0 * 10)/10;
		// CHECK speed != 0....
		if ((potar_0 < 0.1) && (potar_0 > -0.1)) {
			hap_ch1.stop();
		}
		if (potar_0 != factor_speed_ch1 * speed_ch1) {
		    speed_ch1 = factor_speed_ch1 * smoothing_speed(potar_0,3);
			speed_ch1 = factor_speed_ch1*tan(potar_0); // use tan function to smooth the curve
			hap_ch1.setSpeed(speed_ch1);
			if (!hap_ch1.isPlaying()) {
				hap_ch1.stop();
			}
		}
		//}
    }
    
	//cout << potValue << endl;
    if (pinNum == 0) {
		potar_0 = ofMap(ard.getAnalog(pinNum), 0,1024,-2,2);
		//if ((int)potar_0*10 != (int)speed_ch1 * 10) {
		potar_0 = roundf(potar_0 * 10)/10;
		// CHECK speed != 0....
		if ((potar_0 < 0.1) && (potar_0 > -0.1)) {
			potar_0 = 0.1;
		}
		if (potar_0 != factor_speed_ch1 * speed_ch1) {
			speed_ch1 = factor_speed_ch1*potar_0;
			hap_ch1.setSpeed(speed_ch1);
		}
		//}
    }
	if (pinNum == 1) {
		potar_1 = ofMap(ard.getAnalog(pinNum), 0, 1024, -2, 2);
		//if ((int)potar_1 * 10 != (int)speed_ch2 * 10) {
		potar_1 = roundf(potar_1 * 10) / 10;
		// CHECK speed != 0....
		if ((potar_1 < 0.1) && (potar_1 > -0.1)) {
			potar_1 = 0.1;
		}
		if (potar_1 != factor_speed_ch1 * speed_ch2) {
			speed_ch2 = factor_speed_ch1 * potar_1;
			hap_ch2.setSpeed(speed_ch2);
		}
		//}
	}

    if (pinNum == 2) {
		potar_2 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 1);
		//potar_10 = roundf(potar_10 * 10) / 10;
		opBC_ch1 = potar_2;
    }
    if (pinNum == 3) {
		potar_4 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 1);
		//potar_11 = roundf(potar_11 * 10) / 10;
		opBC_ch2 = potar_4;
    }
	if (pinNum == 4) {
		potar_10 = ofMap(ard.getAnalog(pinNum), 0, 1024, -1000, 1000);
		//potar_12 = roundf(potar_12 * 10) / 10;
		x_pos_ch1 = potar_10;
	}
	if (pinNum == 5) {
		potar_11 = ofMap(ard.getAnalog(pinNum), 0, 1024, -1000, 1000);
		x_pos_ch2 = potar_11;
		//potar_14 = roundf(potar_14 * 10) / 10;
		// extension_3channel to add
	}
	if (pinNum == 6) {
		potar_12 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 2);
		//potar_12 = roundf(potar_12 * 10) / 10;
		opBC_ch3 = potar_12;
	}
	if (pinNum == 7) {
		potar_14 = ofMap(ard.getAnalog(pinNum), 0, 1024, 0, 1);
		//potar_14 = roundf(potar_14 * 10) / 10;
		// extension_3channel to add
	}
    // map value between
    // -10 and 10 for potar_0
    // 0 and 255 for potar_2
}

#endif

#ifdef INTERFACE

void ofApp::onButtonEvent(ofxDatGuiButtonEvent e) {
	// Random parameters :: position
	/*if (e.target->getLabel() == "random_pos ch1") {
		doRandom_pos_ch1 = true;
	}*/
}

void ofApp::onToggleEvent(ofxDatGuiToggleEvent e) {
	for (int i = 0; i < number_of_channels; ++i) {
		if (e.target->getLabel() == "Activate " + kanava_gui[i].pos_x_automaton.name_reference) {
			kanava_gui[i].pos_x_automaton.launch = e.target->getChecked();
		}
		if (e.target->getLabel() == "Loop " + kanava_gui[i].pos_x_automaton.name_reference) {
			kanava_gui[i].pos_x_automaton.loop = e.target->getChecked();
		}
		if (e.target->getLabel() == "Reverse " + kanava_gui[i].pos_x_automaton.name_reference) {
			kanava_gui[i].pos_x_automaton.reverse = e.target->getChecked();
		}
	}
}

void ofApp::onSliderEvent(ofxDatGuiSliderEvent e) {
	for (int i = 0; i < number_of_channels; ++i) {
		if (e.target->getLabel() == "Opacity " + ofToString(i + 1)) {
			kanava_variables[i].opacity = int(e.value);
		}
		if (e.target->getLabel() == "Contrast " + ofToString(i + 1)) {
			kanava_variables[i].contrast = int(e.value);
		}
		if (e.target->getLabel() == "Saturation " + ofToString(i + 1)) {
			kanava_variables[i].saturation = int(e.value);
		}
		if (e.target->getLabel() == "X pos " + ofToString(i + 1)) {
			kanava_variables[i].x_pos = int(e.value);
		}
		if (e.target->getLabel() == "Y pos " + ofToString(i + 1)) {
			kanava_variables[i].y_pos = int(e.value);
		}
		if (e.target->getLabel() == "Scale " + ofToString(i + 1)) {
			kanava_variables[i].scale = int(e.value);
		}
		if (e.target->getLabel() == "Fbo width" + ofToString(i + 1)) {
			kanava_variables[i].fbo_width = int(e.value);
			list_of_kanava[i].setFboWidth(kanava_variables[i].fbo_width);
		}
		if (e.target->getLabel() == "Inside X pos" + ofToString(i + 1)) {
			kanava_variables[i].inside_x_pos = int(e.value);
			list_of_kanava[i].setInsideX(kanava_variables[i].inside_x_pos);
		}
		if (e.target->getLabel() == "Crop X" + ofToString(i + 1)) {
			kanava_variables[i].inside_x_crop = int(e.value);
			list_of_kanava[i].setCropX(kanava_variables[i].inside_x_crop);
		}

		/*
		
		kanava_gui[i].geometry->addSlider("" + ofToString(i + 1), 0, 1280, 1280);
		kanava_gui[i].geometry->addSlider("Inside X pos" + ofToString(i + 1), -1280, 1280, 0);
		kanava_gui[i].geometry->addSlider("" + ofToString(i + 1), -1280, 1280, 0);
		kanava_gui[i].geometry->addSlider("" + ofToString(i + 1), -1280, 1280, 0);
		*/

		for (int j = 0; j < 1; ++j) {
			if (e.target->getLabel() == kanava_gui[i].pos_x_automaton.name_reference + " step") {
				kanava_gui[i].pos_x_automaton.step_value = int(e.value);
			}
			if (e.target->getLabel() == kanava_gui[i].pos_x_automaton.name_reference + " speed") {
				kanava_gui[i].pos_x_automaton.speed_value = int(e.value);
			}
		}
	}
}

#endif

inline void ofApp::parameterGUIAutomaton::setupAutomaton(ofxDatGuiFolder** parent, string name, int x, int y, int min=0, int max=100, int value=10)
{
	this->name_reference = name;

	*parent = new ofxDatGuiFolder("Animation " + name);

	ofxDatGuiFolder* tmp_gui = new ofxDatGuiFolder("Animation " + name);
	tmp_gui->addToggle("Activate " + name);
	tmp_gui->addToggle("Reverse " + name);
	tmp_gui->addSlider(name + " speed", min, max, value);
	tmp_gui->addToggle("Loop " + name);

	//tmp_gui->onToggleEvent(this, &ofApp::onToggleEvent);
	//tmp_gui->onSliderEvent(this, &ofApp::onSliderEvent);

	tmp_gui->setPosition(x, y);
	tmp_gui->expand();

	*parent = tmp_gui;
}


void ofApp::parameterGUIAutomaton::setupAutomaton(string name, int x, int y, int min = 0, int max = 100, int value = 10)
{
	this->name_reference = name;

	this->launch = false;
	this->reverse = false;
	this->loop = false;
	this->speed_value = 100;
	this->step_value = 100;

	this->folder = new ofxDatGuiFolder(this->name_reference + " automaton");
	this->folder->addToggle("Activate " + name);
	this->folder->addToggle("Reverse " + name);
	this->folder->addSlider(name + " step", min, max, value); 
	this->folder->addSlider(name + " speed", min, 5*max, 5*value);
	this->folder->addToggle("Loop " + name);

	//this.folder->onToggleEvent(this.folder, &ofApp::onToggleEvent);
	//this.folder->onSliderEvent(this.folder, &ofApp::onSliderEvent);

	this->folder->setPosition(x, y);
	this->folder->expand();
}


void ofApp::channelVariable::init()
{
	this->x_pos = 0;
	this->y_pos = 0;
	this->scale = 100;
	this->opacity = 100;
	this->contrast = 100;
	this->saturation = 100;
	this->fbo_width = 1280;
	this->inside_x_pos = 0;
	this->inside_x_crop = 0;
}
