#pragma once

#include "ofxHapPlayer.h"

class kanava
{
public:
	kanava();
	~kanava();

	void build(int w = 1920, int h = 1080, string path = "rushes/fouquets.mov");
	void update();
	void draw();

	void setSpeed(float value);
	void setInsideX(int value);
	void setOutsideX(int value);
	void setCropX(int value);
	void setFboWidth(int value);

	ofTexture getTexture();

	int main_width, main_height;

private:

	int number_of_channels;

	ofFbo inner_fbo, outer_fbo;
	ofxHapPlayer hap_video;
	ofTexture texture;

	int hap_w, hap_h;
	float speed;
	int x_crop, x_inside, x_outside;

};

