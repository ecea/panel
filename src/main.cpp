#include "ofMain.h"
#include "ofApp.h"

#define WINDOWED

int main()
{
	ofGLWindowSettings settings;
#ifdef WINDOWED
	settings.setSize(4096, 2160);
	settings.windowMode = OF_WINDOW; // OF_FULLSCREEN;// 
#else
	settings.windowMode = OF_FULLSCREEN;
#endif
	settings.setGLVersion(4, 1);

	ofCreateWindow(settings);

	ofRunApp(new ofApp());
}
