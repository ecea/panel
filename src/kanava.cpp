#include "kanava.h"



kanava::kanava()
{
}

kanava::~kanava()
{
}

void kanava::build(int w, int h, string path)
{
	// ALLOCATIONS
	hap_w = w; hap_h = h;
	x_crop = 0; x_outside = 0; x_inside = 0;

	inner_fbo.allocate(hap_w - x_crop, hap_h, GL_RGB);
	outer_fbo.allocate(w, h, GL_RGB);

	hap_video.load(path);
	hap_video.play();
	hap_video.setLoopState(OF_LOOP_NORMAL);

}

void kanava::update()
{
	// first Fbo, for hap file
	inner_fbo.begin();
	ofClear(50, 0, 0);
	hap_video.draw(x_inside, 0);
	inner_fbo.end();

	// second Fbo, to crop - build glitches
	outer_fbo.begin();
	ofClear(0, 50, 0);
	inner_fbo.getTexture().drawSubsection(x_outside, 0, 1920, 1080, x_crop, 0); // Differentiate crop with or without glitches
	outer_fbo.end();
}

void kanava::draw()
{
	outer_fbo.draw(0, 0);
}

void kanava::setSpeed(float value)
{
	speed = value;
	if (speed != 0) {
		hap_video.setSpeed(speed);
	}
	else if (speed == 0) {
		hap_video.setPaused(true);
	}
}

void kanava::setInsideX(int value)
{
	x_inside = value;
}

void kanava::setOutsideX(int value)
{
	// deprecated :: use x_pos in ofApp instead
	x_outside = value;
}

void kanava::setCropX(int value)
{
	x_crop = value;
}

void kanava::setFboWidth(int value) {
	inner_fbo.allocate(value, hap_h, GL_RGB);
}

ofTexture kanava::getTexture() {
	return outer_fbo.getTexture();
}
