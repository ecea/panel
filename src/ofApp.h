#pragma once

#include "ofMain.h"
#include "kanava.h"

#include "ofxHapPlayer.h"
#include "ofxDatGui.h"

class ofApp : public ofBaseApp
{
  
public:
  void setup();
  void update();
  void draw();
  void exit();
    
  void keyPressed(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void windowResized(int w, int h);
  
  int framerate;

  // ARDUINO
  ofArduino	ard;
  bool		bSetupArduino;			// flag variable for setting up arduino once

  struct buttonStruct {
	  int pinNumber;
	  bool isPressed;
	  unsigned long rebounceTime;
	  int clickCount;
  };

  void stateOfButton(buttonStruct *someButton);
  float potar_0, potar_1, potar_2, potar_4, potar_10, potar_11, potar_12, potar_14;
  int buttonNumber;
  buttonStruct * list_of_buttons;

  void onTextInputEvent(ofxDatGuiTextInputEvent e);
  ofTrueTypeFont font;
    
private:

	// DRAW
	kanava * list_of_kanava;

	int number_of_channels;
	ofFbo main_fbo, fbo_frame;

	int main_opacity;
	int blendmode, int old_blendmode;
	int blendmode_transition_actualStep, blendmode_transition_steps;
	int key_hue; float key_value;

	ofShader shader;

	// INTERFACE
	struct parameterGUIAutomaton {
		string name_reference;
		ofxDatGuiFolder* folder;
		ofParameter<bool> launch;
		ofParameter<int> step_value;
		ofParameter<int> speed_value;
		ofParameter<bool> reverse;
		ofParameter<bool> loop;

		void setupAutomaton(ofxDatGuiFolder** parent, string name, int x, int y, int min, int max, int value);
		void setupAutomaton(string name, int x, int y, int min, int max, int value);
	};

	struct KanavaFolder {
		ofxDatGuiFolder* geometry;
		ofxDatGuiFolder* pos_x_animate;
		parameterGUIAutomaton pos_x_automaton;
	};

	struct channelVariable {
		int x_pos, y_pos, scale;
		int opacity, saturation, contrast;
		int fbo_width, inside_x_crop, inside_x_pos;

		void init();
	};

	float animationTime;

	ofxDatGui* kanavaParameters;
	channelVariable* kanava_variables;
	parameterGUIAutomaton* parametersAutomaton;

	KanavaFolder * kanava_gui;

	void changeInsideCrop(int channel, int value);

	void onToggleEvent(ofxDatGuiToggleEvent e); 
	void onButtonEvent(ofxDatGuiButtonEvent e);
	void onSliderEvent(ofxDatGuiSliderEvent e);


	// ARDUINO
	void setupArduino(const int & version);
	void digitalPinChanged(const int & pinNum);
	void analogPinChanged(const int & pinNum);
	void updateArduino();

	string buttonState;
	string potValue;
};
